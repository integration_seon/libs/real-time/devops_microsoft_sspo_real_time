from setuptools import setup, find_packages

setup(
    name='devops_microsoft_sspo_real_time',  # Required
    version='4.4.6',  # Required
    author="Paulo Sergio dos Santo Junior",
    author_email="paulossjuniort@gmail.com",
    description="Possui bibliotecas para realizar a integração entre SSPO e Microsoft DevOps",
    #url="https://github.com/paulossjunior/tfs_integration_seon",
    packages=find_packages(),
    install_requires=['sspo_db', 'tfsx', 'devops_microsoft_mapping_sspo'], 
    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
    setup_requires=['wheel'],
)
